import { beforeEach, describe, expect, it } from "@jest/globals";
import {
  addBug,
  addBugs,
  myError,
  resolvedBug,
  resolvedBugs,
} from "../redux/bug/reducer";
import { apiCall } from "../redux/api";
import store from "../redux/store";
import MockAdapter from "axios-mock-adapter";
import axios from "axios";
describe("bugSlice", () => {
  describe("Action Call bugs", () => {
    let fakeAxios: any;
    beforeEach(() => {
      fakeAxios = new MockAdapter(axios);
    });

    it("CallBugs", async () => {
      const bug = { description: "bug1" };
      const bugSlice = () => store.getState().entities.bugs;
      fakeAxios.onGet("/bugs").reply(200);

      await store.dispatch(addBugs(bug));
      // console.log("😜 ~ file: bug.test.ts:26 ~ it ~ store", store.getState());

      expect(bugSlice().list).toHaveLength(1);
    });
  });
  describe("Action Resolved bugs", () => {
    let fakeAxios: any;
    beforeEach(() => {
      fakeAxios = new MockAdapter(axios);
    });

    it("Resolved bugs", async () => {
      const bug = { description: "bug1" };
      const bugSlice = () => store.getState().entities.bugs;
      fakeAxios.onPatch("/bugs").reply(200);

      await store.dispatch(addBugs(bug));
      expect(bugSlice().list).toHaveLength(1);
    });
  });
  describe("Action add bug", () => {
    let fakeAxios: any;
    beforeEach(() => {
      fakeAxios = new MockAdapter(axios);
    });

    it("addBugs", async () => {
      const bug = { description: "bug1" };
      const bugSlice = () => store.getState().entities.bugs;
      const bugSaved = { ...bug, id: 1, resolved: false };
      fakeAxios.onPost("/bugs").reply(200, bugSaved);

      await store.dispatch(addBugs(bug));

      expect(bugSlice().list).toContainEqual(bugSaved);
    });
  });
  describe("Action resolved Bug", () => {
    it("resolved Bug", () => {
      const resolve = { resolved: true };
      const result = resolvedBugs(1);
      const expected = {
        type: apiCall.type,
        payload: {
          url: `/bugs/1`,
          method: "patch",
          data: resolve,
          onSuccess: resolvedBug.type,
          onError: myError.type,
        },
      };
      expect(result).toEqual(expected);
    });
  });
});
