import { createAction } from "@reduxjs/toolkit";

export const apiCall = createAction<any, string>("api/apiCall");
export const apiCallSuccess = createAction<any | string>("api/apiCallSuccess");
export const apiCallFailed = createAction<any | string>("api/apiCallFailed");
