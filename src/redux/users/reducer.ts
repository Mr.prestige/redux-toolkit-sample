import { createSelector, createSlice } from "@reduxjs/toolkit";

let initialState: any[] = [];
let teamId: number = 0;
const teamSlice = createSlice({
  name: "users",
  initialState,
  reducers: {
    userAdd: (state, action) => {
      state.push({
        userId: ++teamId,
        name: action.payload.name,
      });
    },
    myError: (state, action) => {
      state.push({
        message: "has Error",
      });
    },
  },
});

export const { userAdd, myError } = teamSlice.actions;
export default teamSlice.reducer;
