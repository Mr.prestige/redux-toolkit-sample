const showMessage = (store: any) => (next: any) => (action: any) => {
  action.type === "Project/myError" || action.type === "bug/myError"
    ? console.log(action.payload.message)
    : next(action);
};

export default showMessage;
