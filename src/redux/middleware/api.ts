import axios from "axios";
import * as actions from "../api";
const api =
  ({ dispatch }: { dispatch: any }) =>
  (next: any) =>
  async (action: any) => {
    if (action.type !== actions.apiCall.type) return next(action);

    const { url, method, data, onSuccess, onStart, onError } = action.payload;
    onStart && dispatch({ type: onStart });
    next(action);
    try {
      const response = await axios.request({
        baseURL: "http://localhost:9001/api",
        url,
        method,
        data,
      });
      dispatch({ type: onSuccess, payload: response.data });
    } catch (error: any) {
      dispatch({ type: onError, payload: error.message });
    }
  };

export default api;
