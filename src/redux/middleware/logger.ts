const logger = (param: any) => (state: any) => (next: any) => (action: any) => {
  console.log("😜 ~ file: logger.tsx:2 ~ logger ~ param", param);

  return next(action);
};
export default logger;
