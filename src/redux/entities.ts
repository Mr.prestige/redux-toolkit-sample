import { combineReducers } from "@reduxjs/toolkit";
import bugReducer from "./bug/reducer";
import projectReducer from "./project/reducer";
import teamReducer from "./users/reducer";

export default combineReducers({
  bugs: bugReducer,
  // projects: projectReducer,
  users: teamReducer,
});
