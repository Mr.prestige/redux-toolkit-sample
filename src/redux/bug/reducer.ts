import { createSelector, createSlice } from "@reduxjs/toolkit";
import { apiCall } from "../api";
import * as actions from "../api";
import store from "../store";
import moment from "moment";
import axios from "axios";

interface IinitialState {
  list: any[];
  loading: boolean;
  lastFetch: number;
}
const initialState: IinitialState = {
  list: [],
  loading: false,
  lastFetch: 0,
};

let lastId = 0;

const bugSlice = createSlice({
  name: "bugs",
  initialState,
  reducers: {
    bugRequested: (state, action) => {
      state.loading = true;
    },
    bugRequestedFailed: (state, action) => {
      state.loading = false;
    },
    bugReceived: (state, action) => {
      state.list = action.payload;
      state.loading = false;
      state.lastFetch = Date.now();
    },
    addBug: (state, action) => {
      state.list.push({
        id: ++lastId,
        description: action.payload.description,
        resolved: false,
      });
    },

    resolvedBug: (state, action) => {
      const index = state.list.findIndex((bug) => bug.id === action.payload.id);

      if (index > -1) {
        state.list[index].resolved = true;
      }
    },
    bugAssignToUser: (state, action) => {
      const { id: bugId, userId } = action.payload;
      const index = state.list.findIndex((bug) => bug.id === bugId);
      state.list[index].userId = userId;
    },
    removeBug: (state, action) => {
      state.list.filter((item: any): boolean => item.id !== action.payload.id);
    },
    myError: (state, action) => {
      state.list.push({
        message: action.payload.message,
      });
    },
  },
});
export const {
  addBug,
  resolvedBug,
  bugAssignToUser,
  bugRequested,
  bugReceived,
  myError,
} = bugSlice.actions;

export const loadbugs = () => (dispatch: any, getState: any) => {
  const { lastFetch } = getState().entities.bugs;
  const diffInMinutes = moment().diff(moment(lastFetch), "minute");
  if (diffInMinutes < 10) return;
  dispatch(
    apiCall({
      url: "/bugs",
      onStart: bugRequested.type,
      onSuccess: bugReceived.type,
    })
  );
};

export const addBugs = (bug: any) =>
  apiCall({
    url: "/bugs",
    method: "post",
    data: bug,
    onSuccess: addBug.type,
    onError: myError.type,
  });

export const resolvedBugs = (id: any) =>
  apiCall({
    url: `/bugs/${id}`,
    method: "patch",
    data: { resolved: true },
    onSuccess: resolvedBug.type,
    onError: myError.type,
  });
export const bugAssignToUsers = (bugId: any, userId: any) =>
  apiCall({
    url: `/bugs/${bugId}`,
    method: "patch",
    data: { userId },
    onSuccess: bugAssignToUser.type,
    onError: myError.type,
  });

export const getUnresolved = createSelector(
  (state: { entities: { bugs: any } }) => state.entities.bugs.list,
  (bugs) => bugs.filter((bug: { resolved: any }) => !bug.resolved)
);
export const getUnresolvedUserId = (userId: number) =>
  createSelector(
    (state: { entities: { bugs: any } }) => state.entities.bugs.list,
    (bugs) => bugs.filter((bug: { id: any }) => bug.id === userId)
  );

export default bugSlice.reducer;
