import { configureStore } from "@reduxjs/toolkit";
import reducer from "./reducer";
import showMessage from "./middleware/showMessage";
import api from "./middleware/api";
import logger from "./middleware/logger";
const middlewareArrays = [api];
const store = configureStore({
  reducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(middlewareArrays),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
