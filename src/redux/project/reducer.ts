import { createSelector, createSlice } from "@reduxjs/toolkit";

const initialState: any[] = [];

let lastId = 0;
const projectSlice = createSlice({
  name: "Project",
  initialState,
  reducers: {
    projectAdd: (state, action) => {
      state.push({
        id: ++lastId,
        name: action.payload.name,
      });
    },
    myError: (state, action) => {
      state.push({
        message: action.payload.message,
      });
    },
  },
});

export const { projectAdd, myError } = projectSlice.actions;
export default projectSlice.reducer;
