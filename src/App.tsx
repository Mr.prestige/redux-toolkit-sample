import "./App.css";
import store, { AppDispatch } from "./redux/store";
import React, { useEffect } from "react";
// import { addBug } from "./redux/bug/reducer";

import { projectAdd } from "./redux/project/reducer";
import {
  addBugs,
  bugAssignToUsers,
  getUnresolved,
  loadbugs,
  resolvedBugs,
} from "./redux/bug/reducer";

import showMessage from "./redux/middleware/showMessage";
import { userAdd } from "./redux/users/reducer";
import { useDispatch, useSelector } from "react-redux";

let lastId = 0;
function App() {
  const dispatch: AppDispatch = useDispatch();
  const bugs = useSelector(getUnresolved);

  useEffect(() => {
    dispatch(loadbugs());
    // showMessage(store.getState());
    // store.dispatch(myError({ message: "aaaa" }));
    // setTimeout(() => store.dispatch(addBugs({ description: "sss" })), 2000);
    // setTimeout(() => store.dispatch(bugAssignToUsers(3, 1)), 2000);
    // store.dispatch(userAdd({ name: "Rle" }));
    // store.dispatch(bugAssignToUser({ bugId: 2, userId: 3 }));
    // const bb = getUnresolvedUserId(1)(store.getState());
    // console.log("😜 ~ file: App.tsx:26 ~ useEffect ~ bb", bb);
    // store.dispatch((dispatch: (arg0: { type: string; bug: number[]; }) => void) => {
    // });
  }, []);

  return (
    <div className="App">
      <ul>
        {bugs.map((item: { description: string | number }, index: number) => (
          <li key={index}>{item.description}</li>
        ))}
      </ul>
      <button onClick={() => store.dispatch(projectAdd({ name: "mmm" }))}>
        asdasd
      </button>
    </div>
  );
}

export default App;
